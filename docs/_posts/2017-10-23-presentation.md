---
layout: default
categories: week12
title: "Final Presentation + Future Evaluation"
subtitle: ""
order: 11
---

[Youtube Video of final Prototype in action](https://www.youtube.com/watch?v=NCG5eq4UF2s);


### Pushing this project further

I would love to keep pushing this project further and to keep working on it to come up with a good long lasting solution and there are a few obvious things I would change/consider more going forward.

The first, and biggest change, would be to consider modularity and upgradability.
These two things are really important if I want this to be an item that lasts for a long time in fab lab wgtn. Due to it nature, fab lab wgtn is often changing, adding and updating its sessions and the interactive wall needs to reflect this.

Through making the wall more modular, I think utilising more space would be an interesting change. For this first prototype, I wanted to keep it small and confined but using the entire wall down the hall to fab lab wgtn could be really interesting.
