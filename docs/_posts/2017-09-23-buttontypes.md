---
layout: default
categories: week9
title: "Different Buttons"
subtitle: "Which is the best button for my project"
order: 4
---

## Buttons Types

### Capacitive Buttons

A really strong button type I could use is capacitive buttons. These buttons work by detecting the change in capacitance in this case when a hand touches the sensor.
These buttons allow me to integrate a large number of different buttons in differing locations and with different shapes a sizes.
One draw back to this particular button type is there is not much native feedback. The button pad itself does not click or move at all so all feedback needs to be provided from different sources (LEDs, speaker)

### Tactile Momentary Switches

Tactile Momentary Switches is probably the most common button type and therefore is the most boring.

### Some other sensor

I could try and use a Xbox Kinect sensor or something similar. This would be really cool but also way complicated. 
