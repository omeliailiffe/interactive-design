---
layout: default
categories: week11
title: "Final Layout"
subtitle: ""
order: 9
---

Taking all that I had learnt from the paper prototyping i developed a final layout for the wall, including the final button design.

Below is mockup on illustrator.

![illustrator mockup]({{site.imageurl}}illustratorlayout.png)

For the final functional prototype I will not be including the bottom few branchs of the tree due to the number of capacitive buttons I have.

![illustrator actual mockup]({{site.imageurl}}illustratorlayoutfinal.png)

The final will be made out of a layer of plywood and cardboard with cut outs for the led paths between the buttons.
