---
layout: default
categories: week11
title: "Class Reflection"
subtitle: "from week 11"
order: 8
---

Before class I printed out the finalised buttons to scale and stuck them on the whiteboard.

![finalised layout]({{site.imageurl}}layoutfinal1.jpg)

During class I talked through the layout with Tim and he found it very easy to read and follow.

A couple of justifications are needed for my design desicions:

1. The buttons are arranged from left to right, top to bottom; with the most important (sessions with the more prerequisites) being on the left and the easier processes at the top. It is arranged this way to people new to Fab Lab WGTN are introduced slowly to the machines rather than jumping to the machines that require the most learning first.

![easy to hard]({{site.imageurl}}layoutfinal2.jpg)

2. There are 2 different button types, **sessions** and **machines**. These are differentiated through the rotation of the button. Whilst the **sessions** are orientated portrait, the **machines** are landscape.

![button orientation]({{site.imageurl}}layoutfinal4.png)
