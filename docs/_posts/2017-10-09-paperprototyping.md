---
layout: default
categories: week11
title: "Paper Prototyping"
subtitle: "featuring Liz and Carmen"
order: 7
---

In order to figure out exactly which sessions and machines I would be including the final prototype I started paper prototyping.

I cut out paper circles and labeled each of the buttons that I will be using.
After marking out a 600mm x 1000mm space on the white board I began arranging them in the way I thought would be best.

They were arranged in a logical order left to right (based on requirements for attending/using) and an ease of use order from top to bottom (so the "easiest" sessions/machines were at the top and harder machines at the bottom).

![layouttest1]({{ site.url }}/interactive-design/assets/img/layouttest.jpg)


I got Carman and Liz to review how I had laid it out and as they didn't like it at all they promptly tore it all down and started again. As annoying as this was it allowed me to consider different ways of laying it and see the flaws in my original layout. They arranged the buttons in a much more centered way.

![layouttest2]({{ site.url }}/interactive-design/assets/img/layouttest2.jpg)

Next I asked Wendy to review what we had. She helped me understand that there was certain sessions offered at Fab Lab WGTN that, although many people choose to skip them, were really important to how Fab Lab WGTN operates. These include sessions like Open Blogging (A session on how to document you're work using github), and Open Office Hours (A space for anyone to bring their ideas and get feedback). These sessions are important but not a necessary requirement for using any particular machine so how they are placed on the interactive wall.

![layoutwendy]({{ site.url }}/interactive-design/assets/img/layoutwendy.jpg)
