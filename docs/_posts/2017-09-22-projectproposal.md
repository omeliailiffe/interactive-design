---
layout: default
categories: week8
title: "Proposal"
subtitle: "Initial Proposal and plan"
order: 3
---

### Lets make a plan

## What is an interactive wall

An interactive wall is, simple put, a wall provides information through some user interaction.

For this project, I am to build an interactive wall to layout all (or hopefully, at least most) of the **intro sessions** and **machines/processes** available for use at Fablab WGTN.
This will create a better understanding to both those new to the space and for those who have been there for years.

## Hows it going to work

My initial, always improving/changing, idea is to have a 2 sections to the wall.

#### The Intro sessions
The intro sessions do exactly what they say. They provide an introduction to a certain space, machine, software, skill or process.
A few of the most run intros are:

About Fab
Intro to lasercutting
Intro to 3d printing
Intro to Vcarve
and many more

#### The Machines
Fablab WGTN has a wide range of machines that people can use after doing the introduction session.
These include:

Lasercutter
3d printers
Shopbot
Wax milling
Glitch Knit
Circuit board fabrication (milling and soldering)

The intro sessions and process can be linking up to each of the machines they relate to. By doing this on the wall I can create a tree of intro sessions and machines/processes.

## Interaction

To interact with the wall users will explore the sessions and processes with exist in Fablab WGTN.

An example of an ideal interaction follows:

User pushes laser cutting machine button
The appropriate connected buttons also light up and the path ways between them.
About Fab >> Intro to Lasercutting >> Lasercutter
