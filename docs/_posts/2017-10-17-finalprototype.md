---
layout: default
categories: week12
title: "Assembly the final Prototype for presentation"
subtitle: ""
order: 10
---

The final prototype is made out of a cardboard backing and a laser cut front board.
I have used WS2812B (Addressable LEDS) in a strip cut into sections to provide the lighting for each of the links between the buttons.
The buttons are made out of 1.8mm bamboo ply with touch sensitive felt layered on top to provide the interface. An acrylic circle shows the button label and disperses the light.

### Limitations

The final prototype has a few limitations due to coding and space requirements.
Although the prototype has doesn't have an idle mode, the final would switch back to an idle mode after being left for a certain amount of time (The timer would be long enough to ensure the buttons weren't just being read).
Another limitations is the size of my final prototype. Due to not wanting to make a massive prototype I limited the size down to 900mm. This size limit also means that I was unable to fit all the desired buttons (sessions and machines) on the board and had to choose the most important ones. I decided to leave of the Glitch Knit and the mdx20 as the were both not often the first machines used by new comers to fab lab wgtn and therefore not my target audience.
