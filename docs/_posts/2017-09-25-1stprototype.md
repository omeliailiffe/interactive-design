---
layout: default
categories: week9
title: "First Prototype"
subtitle: "My initial simple prototype"
order: 5
---

My first prototype will be really simple, but will allow me to figure out how I will wire the final and test one button type. It will also allow me to figure out the code for the project.

The prototype will be made out of A3 card, tinfoil (for the buttons), and a few addressable LEDS.

[Here is a video of how the prototype works](https://youtu.be/V06o-o9h4_w)
