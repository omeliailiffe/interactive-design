---
layout: default
categories: week9
title: "Class Feedback and Discussion"
subtitle: "Notes from class"
order: 6
---


Some to the questions that arose in class and my responses.

#### How will I indicate that these "buttons" are buttons.
By having an idle state (leds moving to indicate the interaction) and well as having the buttons made from a different material than everything else on the board. Also providing positive feedback through the leds lighting up when the button is pushed. These things combined with the curious nature of my target audience will allow for the discovery of the non obvious buttons.

#### How will I  differentiate between the different types of buttons (Machines, Sessions)
Not sure this will be super necessary but prehabs by having different buttons shapes. I want to stay away from colour coding them differently as the leds will already be bringing an element of colour and it could confuse the user.

#### Will you leave the exposed wires and leds in your final.
As a user of fab lab wgtn and a coca student I am naturally a really curious person and being able to see those wires and being provided clues on how this was assembled and how it works, I find really amazing. I want users to see the interactive wall and understand after looking closer how it was put together and how it works.
