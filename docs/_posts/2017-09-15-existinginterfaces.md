---
layout: default
categories: week8
title: "Existing interfaces"
subtitle: "An exploration of existing interfaces"
order: 1
---

### iPhone Home Button Analysis

The iPhone home button is an interesting example of interaction design. As the iPhone is used by a wide range of people and, for the most part, defined the category of the smartphone, it was critical that the home button be well thought through.
It was designed to provide a safe, always accessible method to quickly return the user to a familiar area of the device.

##### Interaction
The original home button on the iPhone through till the iPhone 6 was a tactile single push single throw momentary switch (also known as a button).
However post iPhone 6 apple upgraded to a digital button. This digital button provides a pressure sensitive area (in the same place as the original home button) and when enough pressure from the user is provided feedback is given, in the form of Apples Taptic Engine, to indicate the button has been pushed. Whilst this is more complicated than just using a tactile switch there are a number of benefits (durability, extra control, etc) and the untrained user wouldn't notice the difference.

##### Indication that its a button
On the original iPhone the home button was a small dip in the glass front. It also had a small white square.
Post iPhone 5 this was updated to allow for touch id. The button became flat, the square removed and a small raised ring of metal was placed around the button area. This raised metal ring indicates where to push and also acts as a capacitive touch sensor for additional functions.

##### Feedback
In addition to the tactile or haptic feedback, the iPhone also provides visual feedback as it will return the display to the home screen.
