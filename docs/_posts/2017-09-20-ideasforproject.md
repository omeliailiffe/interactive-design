---
layout: default
categories: week8
title: "Ideation"
subtitle: "Potential Ideas for my project"
order: 2
---

### Reborns

An area of interaction that fascinated me (and creeped me out a little to be honest) was reborns.

Reborns are incredibly life like dolls hand crafted to look like babies.
These dolls are looked after by a wide range of people for all sorts of different reasons.
One thing I noticed about these dolls whilst research was that they were really static. Although the were made the same size and weight as real babies they didn't have any ability to interact/respond to anything. I thought it would be quite an interesting project to push these baby dolls further into the uncanny valley by making them react to touch, and the world around them.

### Better Wifi Router / Wifi diagnosing device

For the entire year I have suffered with bad internet and no one can quite work out how to fix it.
I really liked what google did with their wifi routers last year as they made them visually appealing. This encouraged people not to hide away there routers behind the fridge but to leave them out on display thus improving the signal

I thought it would be interesting to redesign a wifi router to make it beautiful but also redesign it to allow users to understand what was happening with their network at a glance.

### Interactive Wall

My final idea, and the idea I have selected to continue with, is the interactive wall.
In Fablab WGTN the only way in is through dimly light, slightly scary hallway. Wendy, Craig and the Winterns are begin a project to help brighten the space up and improve first impressions and engagement with first time visitors to  Fablab WGTN.
I have come on board with the project to design and make an interactive wall. This wall provide a tactile interactive element to inform visitors of Fablab WGTN more about the space. 
