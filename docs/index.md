---
layout: default
---

I have started this project as a part of my interactions and interfaces course at Massey University but I plan to extend the duration of the project longer than 6 weeks. During the duration of my course I will be updating this page with a log of my process, user testing and thoughts in general.

Please check out the posts on the left and follow my progress. :)

![image1]({{site.imageurl}}1.png)
![image2]({{site.imageurl}}2.png)
![image3]({{site.imageurl}}3.png)

[Youtube Video of final Prototype in action](https://www.youtube.com/watch?v=NCG5eq4UF2s);
